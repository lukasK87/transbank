<?php
require_once(__DIR__.'/vendor/autoload.php');

use Transbank\Webpay\Configuration;
use Transbank\Webpay\Webpay;

class transbank extends PaymentModule
{

    use \Components\Traits\LoggerTrait;

    /**
     * Module version. Make sure to increase version any time you add some
     * new functions in this class!
     * @var string
     */
    protected $version = '1.2019-05-29';

    /**
     * Module name, visible in admin portal.
     * @var string
     */
    protected $modname = 'Transbank Webpay Plus';

    /**
     * Module description, visible in admin portal
     * @var string
     */
    protected $description = 'Transbank Webpay Plus Online Checkout module';


    protected $supportedCurrencies = ['USD', 'CLP', 'UF'];

    protected $configuration = [

        'Commerce Code' => [
            'value' => '',
            'type' => 'input'
        ],

        'Public Certificate' => [
            'value' => '',
            'type' => 'input'
        ],

        'Private Key' => [
            'value' => '',
            'type' => 'input'
        ],

        'Test mode' => [
            'value' => '',
            'description' => 'Tick if your Transbank Webpay Plus environment is Integration',
            'type' => 'check'
        ],



    ];


    public function __construct()
    {

        parent::__construct();

    }


    /**
     * HostBill will run this function first time module is activated.
     * Use it to create some database tables
     */
    public function install()
    {

    }

    /**
     * HostBill will run this function when admin choose to "Uninstall" module.
     * Use it to truncate/remove tables.
     */
    public function uninstall()
    {

    }

    /**
     * Return HTML code that should be displayed in clientarea for client to pay (ie. in invoice details)
     * @return string
     */

    public function prepareConfiguration(){
        if($this->configuration['Test mode']['value']) {
            return Configuration::forTestingWebpayPlusNormal();
        }
        else{
            $configuration = new Configuration();
            $configuration->setEnvironment("PRODUCCION");
            $configuration->setCommerceCode($this->configuration['Commerce Code']['value']);
            $configuration->setPrivateKey($this->configuration['Private Key']['value']);
            $configuration->setPublicCert($this->configuration['Public Certificate']['value']);
            return $configuration;
        }

    }

    public function getTransaction($type = 1){
        $configuration = $this->prepareConfiguration();

        if($type!=1)
            $transaction = (new Webpay($configuration))->getNullifyTransaction();
        else
            $transaction = (new Webpay($configuration))->getNormalTransaction();


        return $transaction;
    }

    public function drawForm(){

        $security_token = Tokenizer::getSalt();

        if (!$this->checkFields()) {
            return false;
        }

        $transaction = $this->getTransaction();

        if(!($transaction instanceof Transbank\Webpay\WebPayNormal)){
            $this->logActivity([
                'output' => ['error' => 'Cannot create WebPayNormal object'],
                'result' => self::PAYMENT_FAILURE
            ]);
            $this->addError("Payment failed please contact administrator");

            return false;
        }



        $invoice = Invoice::createInvoice($this->invoice_id);
        $amount = $invoice->getTotal();
        $sessionId = $security_token;
        $buyOrder = $this->invoice_id;
        $returnUrl =Utilities::url('?cmd=transbank&action=pay&security_token='.$security_token.'&invoice_id='.$this->invoice_id);
        $finalUrl =Utilities::url('?cmd=clientarea&action=invoice&id='.$this->invoice_id);

        $initResult = $transaction->initTransaction($amount, $buyOrder, $sessionId, $returnUrl, $finalUrl);


        if(is_array($initResult)){
            $this->logActivity([
                'output' => ['error' => $initResult['detail'] ? $initResult['detail'] : 'Form generating error'],
                'result' => self::PAYMENT_FAILURE
            ]);

            $this->addError($initResult['detail'] ? $initResult['detail'] : 'Error, please contact administrator');
            return false;
        }

        $formAction = $initResult->url;
        $tokenWs = $initResult->token;

        $form ='<form action="'.$formAction.'" method="post" name="payform">';
        $form .= '<input type="hidden" name="token_ws" value="'.$tokenWs.'">';
        $form .= '<input type="submit" value="'.$this->paynow_text().'" />';
        $form .= '</form>';
        return $form;

    }

    public function checkPayment($params){


        $transaction  = $this->getTransaction();
        $result = $transaction->getTransactionResult($params["token_ws"]);



        if(is_array($result)){

            $this->logActivity([
                'output' => ['error' => $result['error'].', detail: '.$result['detail'] ],
                'result' => self::PAYMENT_FAILURE
            ]);

            $this->addError($result['error']);
            return false;
        }


        $output = $result->detailOutput;


        if ($output->responseCode == 0) {

            $transaction_params = [
                'client_id' => $this->client['id'],
                'invoice_id' => $this->invoice_id,
                'description' => 'Transbank WebPay payment for Invoice no. '.$output->buyOrder,
                'transaction_id' => $output->authorizationCode,
                'in' => $output->amount,
                'fee' => '0'
            ];

            $this->logActivity([
                'output' => 'Invoice no. '.$output->buyOrder,
                'result' => self::PAYMENT_SUCCESS
            ]);

            $this->addTransaction($transaction_params);
            return $result->urlRedirection;
        }
        else{

            $this->logActivity([
                'output' => ['error' => $output->responseDescription ],
                'result' => self::PAYMENT_FAILURE
            ]);

            $this->addError($output->responseDescription);
        }


      return false;

    }


        /**
     * Handle payment refund
     *
     * @param $_transaction
     */
    public function refund($_transaction, $amount) {

        $transaction  = $this->getTransaction(2);

        $invoice = Invoice::createInvoice($this->invoice_id);
        $authorizedAmount = $invoice->getTotal();
        $result = $transaction->nullify($_transaction, $authorizedAmount, $this->invoice_id, $amount, NULL);

        if(is_array($result)){
            $this->logActivity(array(
                'output' => ['error' => $result['error'].', detail: '.$result['detail']],
                'result' => self::PAYMENT_FAILURE
            ));

            return false;
        }
        elseif($result->nullifiedAmount){
            $refund = $this->addRefund(array(
                'target_transaction_number' => $_transaction,
                'transaction_id' => $result->authorizationCode,
                'amount' => $result->nullifiedAmount
            ));

            $this->logActivity(array(
                'output' => (array)$result,
                'result' => $refund ? self::PAYMENT_SUCCESS : self::PAYMENT_FAILURE
            ));
            return $refund;
        }

        $this->logActivity(array(
            'output' => ['error' => 'Unknown error'],
            'result' => self::PAYMENT_FAILURE
        ));

        return false;
    }
    

}





