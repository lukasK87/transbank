
<div style="text-align:center;">
    <form action="{$url}" method="post">

        <input type="hidden" name="token_ws" value="{$token_ws}">
        <input type="submit" id="redirect" class="btn-primary" value="Next">
    </form>
</div>
{literal}
<script>
    $(function() {
        $('#redirect').click();
    });
</script>
{/literal}