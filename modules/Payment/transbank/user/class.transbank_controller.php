<?php

class transbank_controller extends HBController {

    /**
     *
     * @var rave
     */
    var $module;

    /**
     *
     * @var UserAuthorization
     */
    var $authorization;

    /**
     * ?cmd=rave&action=pay&txref=HB_1_1_1111
     * @param <type> $params
     */


    public function pay($params) {

        if (!isset($params['invoice_id']) || !$this->authorization->get_login_status()) {
            Utilities::redirect('?cmd=root');
        }

        $client_id = $this->authorization->get_id();
        $invoice = Invoice::createInvoice($params['invoice_id']);

        if ( $invoice->getInvoiceId() != $params['invoice_id'] || $invoice->getClientId() != $client_id) {
            Utilities::redirect('?cmd=root');
        }

        $client = HBLoader::LoadModel('Clientarea');
        $this->module->setClient($client->get_client_details($client_id));
        $this->module->setInvoice($params['invoice_id']);
        $this->module->setCurrency($invoice->getCurrency());

        $url = $this->module->checkPayment($params);
        if($url){
            $this->template->assign(['url' => $url,  'token_ws' => $params["token_ws"]]);

            $this->template->render(APPDIR_MODULES.'Payment/transbank/template/finish_form.tpl', [], 1);

        } else {
                Utilities::redirect('?cmd=clientarea&action=invoices');
        }



    }


}