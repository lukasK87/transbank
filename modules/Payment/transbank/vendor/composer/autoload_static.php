<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit5c72a7c9bfee0d90c38999042bc5b959
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Transbank\\Webpay\\' => 17,
            'Transbank\\Onepay\\Exceptions\\' => 28,
            'Transbank\\Onepay\\' => 17,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Transbank\\Webpay\\' => 
        array (
            0 => __DIR__ . '/..' . '/transbank/transbank-sdk/lib/webpay',
            1 => __DIR__ . '/..' . '/transbank/transbank-sdk/lib/webpay/soap',
        ),
        'Transbank\\Onepay\\Exceptions\\' => 
        array (
            0 => __DIR__ . '/..' . '/transbank/transbank-sdk/lib/onepay/exceptions',
        ),
        'Transbank\\Onepay\\' => 
        array (
            0 => __DIR__ . '/..' . '/transbank/transbank-sdk/lib/onepay',
            1 => __DIR__ . '/..' . '/transbank/transbank-sdk/lib/onepay/utils',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit5c72a7c9bfee0d90c38999042bc5b959::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit5c72a7c9bfee0d90c38999042bc5b959::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
